#include "lifx-payload-dtos.h"

LIFXLightState::LIFXLightState(uint16_t _hue, uint16_t _saturation, uint16_t _brightness, uint16_t _kelvin)
{
  hue = _hue;
  saturation = _saturation;
  brightness = _brightness;
  kelvin = _kelvin;
}

LIFXPowerState::LIFXPowerState(uint16_t _level)
{
  level = _level;
}