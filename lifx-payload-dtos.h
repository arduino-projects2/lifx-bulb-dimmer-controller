/*
  A collection of classes to represent the important data within a LIFX packet's payload.
*/

#ifndef LIFXPayloadWrappers_h
#define LIFXPayloadWrappers_h

#include <stdint.h>

class LIFXLightState
{
public:
  uint16_t hue;
  uint16_t saturation;
  uint16_t brightness;
  uint16_t kelvin;
  LIFXLightState(uint16_t hue, uint16_t saturation, uint16_t brightness, uint16_t kelvin);
};

class LIFXPowerState
{
public:
  uint16_t level;
  LIFXPowerState(uint16_t level);
};
#endif