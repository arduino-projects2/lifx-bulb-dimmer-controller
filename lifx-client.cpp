#include "lifx-client.h"

/**
 * @brief Converts a boolean representing if a LIFX device is on or off to a LIFX uint16
 *
 * @param powerIsOn A boolean representing a device's on/off state
 * @return uint16_t A LIFX level number
 */
uint16_t LIFXClient::convertBooleanToPowerLevel(bool powerIsOn)
{
  return powerIsOn ? LIFX_POWER_LEVEL_ON : LIFX_POWER_LEVEL_OFF;
}

/**
 * @brief Converts a LIFX uint16 to a boolean, to represent if a light is off or on
 *
 * @param level A power state expressed as a LIFX level
 * @return true When a the power state corresponds to on
 * @return false When a the power state corresponds to off
 */
bool LIFXClient::convertPowerLevelToBoolean(uint16_t level)
{
  return level > LIFX_POWER_LEVEL_OFF;
}

/**
 * @brief Converts a LIFX brightness level to a percentage (0-100%)
 *
 * @param brightness A brightness number expressed as a LIFX level
 * @return unsigned int The brightness as a percentage
 */
unsigned int LIFXClient::convertBrightnessLevelToPercentage(uint16_t brightness)
{
  return ((brightness - LIFX_LIGHT_MIN_BRIGHTNESS) * 100) / (LIFX_LIGHT_MAX_BRIGHTNESS - LIFX_LIGHT_MIN_BRIGHTNESS);
}

LIFXClient::LIFXClient(WiFiUDP _wifiUdp, uint32_t _source)
{
  wifiUdp = _wifiUdp;
  source = _source;

  // Setup incoming UDP packet listener
  wifiUdp.begin(localPort);
}

/**
 * @brief Creates a new LIFX packet header with an incremented serial and some prepulated required fields
 *
 * @param destinationMac The unique hardware identifier for the bulb to communicate with
 * @param size The size of the entire packet (header and any payload)
 * @param res_required 0 is recommended when using Set messages. 0 is also recommended for Get messages, since they return State messages anyway
 * @param ack_required 1 is recommended when using Set messages. 0 is recommended for Get messages
 * @param type The LIFX type of the packet
 * @return lifx_header An instance of the packet structure
 */
lifx_header LIFXClient::createHeader(uint8_t *destinationMac, uint16_t size, uint8_t res_required, uint8_t ack_required, uint16_t type)
{

  // LIFX sequence number should cycle between 0-255
  if (currentSequence >= 255)
  {
    currentSequence = 0;
  }
  else
  {
    // Increment sequence
    currentSequence++;
  }

  // Initialise structure
  lifx_header newHeader;
  memset(&newHeader, 0, sizeof(newHeader));

  // Set the target a memory-safe way
  memcpy(newHeader.target, destinationMac, sizeof(uint8_t) * LIFX_SERIAL_LEN);

  // Setup the header
  newHeader.size = size;
  newHeader.tagged = 0;
  newHeader.addressable = 1;
  newHeader.protocol = 1024;
  newHeader.source = source;
  newHeader.ack_required = ack_required;
  newHeader.res_required = res_required;
  newHeader.sequence = currentSequence;
  newHeader.type = type;

  return newHeader;
}

/**
 * @brief Recieves light on/off state
 *
 * @param destinationMac The unique hardware identifier for the bulb to communicate with
 */
void LIFXClient::requestPower(uint8_t *destinationMac)
{
  lifx_header header = createHeader(destinationMac, sizeof(lifx_header), 0, 0, LIFX_DEVICE_GETPOWER);

  wifiUdp.beginPacket(broadcast_ip, LIFX_PORT);
  wifiUdp.write((char *)&header, sizeof(lifx_header));
  wifiUdp.endPacket();
}

/**
 * @brief Sets light to on or off
 *
 * @param destinationMac The unique hardware identifier for the bulb to communicate with
 * @param level The power state, using a LIFX level number
 */
void LIFXClient::setPower(uint8_t *destinationMac, uint16_t level)
{
  lifx_payload_device_set_power payload;
  memset(&payload, 0, sizeof(payload));

  lifx_header header = createHeader(destinationMac, sizeof(lifx_header) + sizeof(payload), 0, 1, LIFX_DEVICE_SETPOWER);

  // Setup payload
  payload.level = level;

  // Send packet
  wifiUdp.beginPacket(broadcast_ip, LIFX_PORT);
  wifiUdp.write((byte *)&header, sizeof(lifx_header)); // Treat the structures like byte arrays
  wifiUdp.write((byte *)&payload, sizeof(payload));    // Add payload bytes to end of packet
  wifiUdp.endPacket();
}

/**
 * @brief Recieves light color data
 *
 * @param destinationMac The unique hardware identifier for the bulb to communicate with
 */
void LIFXClient::requestColor(uint8_t *destinationMac)
{
  lifx_header header = createHeader(destinationMac, sizeof(lifx_header), 0, 0, LIFX_DEVICE_GETCOLOR);

  wifiUdp.beginPacket(broadcast_ip, LIFX_PORT);
  wifiUdp.write((char *)&header, sizeof(lifx_header));
  wifiUdp.endPacket();
}

/**
 * @brief Processes any new LIFX packets that have been received
 */
void LIFXClient::processPackets()
{
  unsigned int packetLength = wifiUdp.parsePacket();

  // Quit if no/an invalid packet is received
  if (packetLength > LIFX_INCOMING_PACKET_BUFFER_LEN || packetLength < sizeof(lifx_header))
  {
    return;
  }

  // Read packet data
  byte buffer[LIFX_INCOMING_PACKET_BUFFER_LEN];
  wifiUdp.read(buffer, sizeof(buffer));

  // Read header bytes into structure
  lifx_header packetHeader;
  memcpy(&packetHeader, buffer, sizeof(packetHeader));

  // Assemble packet into relevant structure for type and call its handler
  switch (packetHeader.type)
  {
  case LIFX_DEVICE_STATEPOWER:
    _deviceStatePowerHandler(
        packetHeader,
        new LIFXPowerState(
            ((lifx_payload_device_state_power *)(sizeof(lifx_header) + buffer))->level));
    break;
  case LIFX_DEVICE_STATELIGHT:
    _deviceStateLightHandler(
        packetHeader,
        new LIFXLightState(
            ((lifx_payload_device_state_color *)(sizeof(lifx_header) + buffer))->hue,
            ((lifx_payload_device_state_color *)(sizeof(lifx_header) + buffer))->saturation,
            ((lifx_payload_device_state_color *)(sizeof(lifx_header) + buffer))->brightness,
            ((lifx_payload_device_state_color *)(sizeof(lifx_header) + buffer))->kelvin));
    break;
  case LIFX_DEVICE_ACKNOWLEDGE:
    _deviceAcknowledgeHandler(packetHeader);
    break;
  default:
    Serial.printf("ERROR: Packet type is unsupported/unexpected (%d)!\n", packetHeader.type);
    break;
  }

  // Run recursively until no new packets are left unprocessed
  processPackets();
}

void LIFXClient::setDeviceStatePowerHandler(void (*pointerToHandler)(lifx_header, LIFXPowerState *))
{
  _deviceStatePowerHandler = pointerToHandler;
}

void LIFXClient::setDeviceStateLightHandler(void (*pointerToHandler)(lifx_header, LIFXLightState *))
{
  _deviceStateLightHandler = pointerToHandler;
}

void LIFXClient::setDeviceAcknowledgeHandler(void (*pointerToHandler)(lifx_header))
{
  _deviceAcknowledgeHandler = pointerToHandler;
}

/**
 * @brief Sets light colour and brightness
 *
 * @param destinationMac The unique hardware identifier for the bulb to communicate with
 * @param hue
 * @param saturation
 * @param brightness
 * @param kelvin
 * @param duration Speed of the light change transition animation
 */
void LIFXClient::setColor(uint8_t *destinationMac, uint16_t hue, uint16_t saturation, uint16_t brightness, uint16_t kelvin, uint32_t duration)
{
  lifx_payload_device_set_color payload;
  memset(&payload, 0, sizeof(payload));

  lifx_header header = createHeader(destinationMac, sizeof(lifx_header) + sizeof(payload), 0, 1, LIFX_DEVICE_SETCOLOR);

  // Setup payload
  payload.hue = hue;
  payload.saturation = saturation;
  payload.brightness = brightness;
  payload.kelvin = kelvin;
  payload.duration = duration;

  // Send packet
  wifiUdp.beginPacket(broadcast_ip, LIFX_PORT);
  wifiUdp.write((byte *)&header, sizeof(lifx_header)); // Treat the structures like byte arrays
  wifiUdp.write((byte *)&payload, sizeof(payload));    // Add payload bytes to end of packet
  wifiUdp.endPacket();
}