// Your network SSID (name)
char ssid[] = "a wifi name";

// Your network password (use for WPA, or use as key for WEP)
char pass[] = "my password 123";

// Bulb's MAC address
uint8_t destinationMac[] = {0xd0, 0x73, 0xd5, 0x5b, 0x8c, 0xe5};

// Unique identifier for the light switch
uint32_t switchSource = 1;