# LIFX dimmable button ESP8266
A dial with a button to toggle or tune a LIFX bulb's brightness.

Logs WiFi connection phase, calibration and usage to serial monitor for debugging.

Uses Brian Low's [Rotary library](https://github.com/brianlow/Rotary) to debounce (avoid glitches when spinning the dial).
Project initially inspired by [Daniel Hall's post](https://community.lifx.com/t/sending-lan-packet-using-arduino/1460/7) on the LIFX forum.

See the [LIFX docs](https://lan.developer.lifx.com/docs/packet-contents) for more info on LIFX packet structures.

The bulb's Mac address can be found in the LIFX app. This can be put raw into the code.
<br/>
For example, `d073d55b8ce7`, would be:<br/>
`{0xd0, 0x73, 0xd5, 0x5b, 0x8c, 0xe7}`

NOTE: Most LIFX Mac addresses will look similar to the above example.

## Secrets.h
To setup a connection, ensure you create a `secrets.h` file. See `secrets EXAMPLE.h` for an example.

