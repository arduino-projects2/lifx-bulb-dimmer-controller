#include <ESP8266WiFi.h>
#include <Rotary.h>
#include <WiFiUdp.h>
#include "lifx-client.h"
#include "lifx-payload-dtos.h"
#include "secrets.h"

#define ROTARY_DT_PIN 12
#define ROTARY_CLK_PIN 13
#define ROTARY_SWITCH_PIN 0

LIFXClient *lifxClient;
Rotary rotaryEncoder = Rotary(ROTARY_DT_PIN, ROTARY_CLK_PIN);

// A networking instance to let us send and receive packets over UDP
WiFiUDP wifiUdp;

// Confirmed bulb settings (from acknowledgements and getters)
LIFXPowerState *currentPowerState;
LIFXLightState *currentLightState;

// Expected bulb settings (local changes made after last response from bulb)
LIFXPowerState *cachedPowerState;
LIFXLightState *cachedLightState;

uint8_t lastReceivedSetPowerSequence;
uint8_t lastSentSetPowerSequence;
uint32_t lastSentSetPowerTime = 0;
uint8_t lastReceivedSetColorSequence;
uint8_t lastSentSetColorSequence;
uint32_t lastSentSetColorTime = 0;

int lastSwitchValue;
unsigned long lastLightToggle = 0;
unsigned long lastLightCheck = 0;
unsigned long lastLightInteraction = 0;
unsigned long lastSuccessfulCommunication = 0; // Tracks when the switch last worked
void (*reset)(void) = 0;

// Settings
const unsigned long lightToggleFrequency = 150; // How often the button can be clicked to toggle the light (prevents glitches)
const int brightnessSteps = 16;									// How many turns to go from minimum to max brightness (and vice-versa)
const int stepSize = (LIFX_LIGHT_MAX_BRIGHTNESS - LIFX_LIGHT_MIN_BRIGHTNESS) / brightnessSteps;
uint32_t lightChangeDuration = 100;							 // Light's brightness change animation duration (milliseconds)
const unsigned long lightCheckFrequency = 10000; // Milliseconds between light status checks
const unsigned long rebootThreshold = 30000;		 // How long without bulb responses before the switch should reboot itself, in case of error
const uint32_t setterConsideredFailTime = 300;	 // How long to wait (milliseconds) before repeating a set command
const int maxRetries = 5;												 // How many times to keep trying a setter on an unresponsive bulb
const uint32_t setterRetryTimeout = setterConsideredFailTime * maxRetries;

void setup()
{
	Serial.begin(115200);
	Serial.println("Starting...");

	// Setup pins for internal LED
	pinMode(LED_BUILTIN, OUTPUT);
	digitalWrite(LED_BUILTIN, HIGH);

	// Setup rotary encoder hardware
	rotaryEncoder.begin();
	pinMode(ROTARY_SWITCH_PIN, INPUT_PULLUP);
	lastSwitchValue = digitalRead(ROTARY_SWITCH_PIN);

	// Connect to WiFi network
	wifiSetUp();

	// Turn LED off
	digitalWrite(LED_BUILTIN, LOW);

	lifxClient = new LIFXClient(wifiUdp, switchSource);

	// Setup callbacks
	lifxClient->setDeviceStatePowerHandler(powerStateHandler);
	lifxClient->setDeviceStateLightHandler(lightStateHandler);
	lifxClient->setDeviceAcknowledgeHandler(deviceAcknowledgeHandler);
}

void powerStateHandler(lifx_header packetHeader, LIFXPowerState *powerState)
{
	if (packetHeader.source != switchSource)
		return;

	Serial.printf(
			"Power state received: %s\n",
			LIFXClient::convertPowerLevelToBoolean(powerState->level) ? "ON" : "OFF");

	cachedPowerState = powerState; // Update cache
	lastSuccessfulCommunication = millis();
}

void lightStateHandler(lifx_header packetHeader, LIFXLightState *lightState)
{
	if (packetHeader.source != switchSource)
		return;

	Serial.printf(
			"Light state received: %d%%\n",
			LIFXClient::convertBrightnessLevelToPercentage(lightState->brightness));

	cachedLightState = lightState; // Update cache
	lastSuccessfulCommunication = millis();
}

void deviceAcknowledgeHandler(lifx_header packetHeader)
{
	if (packetHeader.source != switchSource)
		return;

	Serial.printf("Setter acknowledgement received (sequence: %d)\n", packetHeader.sequence);
	lastSuccessfulCommunication = millis();

	if (packetHeader.sequence == lastSentSetPowerSequence)
	{
		lastReceivedSetPowerSequence = packetHeader.sequence;
	}
	else if (packetHeader.sequence == lastSentSetColorSequence)
	{
		lastReceivedSetColorSequence = packetHeader.sequence;
	}
	else
	{
		Serial.printf("Received acknowledgement for unknown or superseeded setter (sequence: %d)\n", packetHeader.sequence);
	}
}

void loop()
{
	const bool switchFellOffline = lastSuccessfulCommunication != 0 && millis() - lastSuccessfulCommunication > rebootThreshold;
	if (switchFellOffline)
	{
		Serial.println("ERROR: Switch fell offline, resetting Arduino...");
		reset();
	}

	const bool isUserUsingSwitch = millis() - lastLightInteraction < lightCheckFrequency;
	const bool hasCacheExpired = millis() - lastLightCheck > lightCheckFrequency;
	const bool noCacheSaved = lastLightCheck == 0;
	if (noCacheSaved || (hasCacheExpired && !isUserUsingSwitch))
	{
		// Requests bulb states for caching
		// If cached, the bulb doesn't need to be polled for its state/colour
		// when a user wants to "toggle" or "add 10% brightness"
		lastLightCheck = millis();
		lifxClient->requestPower(destinationMac);
		lifxClient->requestColor(destinationMac);
	}

	const bool setPowerPacketDropped = lastSentSetPowerSequence != lastReceivedSetPowerSequence &&
																		 millis() - lastSentSetPowerTime > setterConsideredFailTime;
	if (setPowerPacketDropped)
	{
		lifxClient->setPower(destinationMac, cachedPowerState->level);
	}

	lifxClient->processPackets();

	switch (rotaryEncoder.process())
	{
	case DIR_CW:
		handleDialClockwise();
		break;
	case DIR_CCW:
		handleDialCounterclockwise();
		break;
	}

	processClicks(digitalRead(ROTARY_SWITCH_PIN));
}

void handleDialClockwise()
{
	lastLightInteraction = millis();
	Serial.println("Clockwise");

	// Block if light data hasn't been fetched
	if (cachedPowerState == NULL || cachedLightState == NULL)
		return;

	const bool isLightOn = LIFXClient::convertPowerLevelToBoolean(cachedPowerState->level);

	// Block if light already at max volume (avoids packet spam)
	if (isLightOn && cachedLightState->brightness == LIFX_LIGHT_MAX_BRIGHTNESS)
		return;

	uint16_t newBrightness;
	if (!isLightOn)
	{
		// When light is brightening from off, set it to the minimum dimmed amount
		newBrightness = LIFX_LIGHT_MIN_BRIGHTNESS + stepSize;
	}
	else
	{
		const uint16_t currentBrightness = cachedLightState->brightness;
		newBrightness = constrain(currentBrightness + stepSize, LIFX_LIGHT_MIN_BRIGHTNESS, LIFX_LIGHT_MAX_BRIGHTNESS);
	}

	Serial.printf(
			"Clockwise (light was %d%%, changing to %d%%)...\n",
			LIFXClient::convertBrightnessLevelToPercentage(cachedLightState->brightness),
			LIFXClient::convertBrightnessLevelToPercentage(newBrightness));

	// Update cache
	cachedLightState->brightness = newBrightness;

	// Update light
	lifxClient->setColor(
			destinationMac,
			cachedLightState->hue,
			cachedLightState->saturation,
			newBrightness,
			cachedLightState->kelvin,
			lightChangeDuration);

	// If light was brightened from an off-state, turn it on
	if (!isLightOn)
	{
		uint8_t onLevel = LIFXClient::convertBooleanToPowerLevel(true);
		lifxClient->setPower(destinationMac, onLevel);

		// Update cache
		cachedPowerState->level = onLevel;
	}
}

void handleDialCounterclockwise()
{
	lastLightInteraction = millis();
	Serial.println("Counterclockwise");

	// Block if light data hasn't been fetched
	if (cachedPowerState == NULL || cachedLightState == NULL)
		return;

	// Block dimming if light is off
	const bool isLightOn = LIFXClient::convertPowerLevelToBoolean(cachedPowerState->level);
	if (!isLightOn)
	{
		return;
	}

	const uint16_t currentBrightness = cachedLightState->brightness;
	uint16_t newBrightness = constrain(currentBrightness - stepSize, LIFX_LIGHT_MIN_BRIGHTNESS, LIFX_LIGHT_MAX_BRIGHTNESS);

	Serial.printf(
			"Counterclockwise (light was %d%%, changing to %d%%)...\n",
			LIFXClient::convertBrightnessLevelToPercentage(cachedLightState->brightness),
			LIFXClient::convertBrightnessLevelToPercentage(newBrightness));

	// Update cache
	cachedLightState->brightness = newBrightness;

	// Update light
	lifxClient->setColor(
			destinationMac,
			cachedLightState->hue,
			cachedLightState->saturation,
			newBrightness,
			cachedLightState->kelvin,
			lightChangeDuration);

	// Turn off the bulb if the brightness is set to the minimum (0%)
	if (newBrightness == LIFX_LIGHT_MIN_BRIGHTNESS)
	{
		uint8_t offLevel = LIFXClient::convertBooleanToPowerLevel(false);
		lifxClient->setPower(destinationMac, offLevel);
		cachedPowerState->level = offLevel;
	}
}

// Calls handlers when button is first pressed or released
void processClicks(int switchValue)
{
	// Change LED with button state
	digitalWrite(LED_BUILTIN, switchValue);

	// If button changed value
	if (switchValue != lastSwitchValue)
	{
		// Handle event
		switch (switchValue)
		{
		case HIGH:
			handleButtonDown();
			break;
		default:
			handleButtonUp();
			break;
		}
	}

	lastSwitchValue = switchValue;
}

void handleButtonUp() {}

void handleButtonDown()
{
	lastLightInteraction = millis();
	Serial.println("Clicked rotary button!");

	// Block if light data hasn't been fetched
	if (cachedPowerState == NULL || cachedLightState == NULL)
	{
		Serial.println("ERROR: Light can't be toggled, current light setting is unknown");
		return;
	}

	if (millis() - lastLightToggle < lightToggleFrequency)
	{
		Serial.println("WARN: Ignored button toggle spam");
		return;
	}

	const bool isLightOn = LIFXClient::convertPowerLevelToBoolean(cachedPowerState->level);
	uint16_t newLevel = LIFXClient::convertBooleanToPowerLevel(!isLightOn);

	// If being switched on, set the brightness to 100%
	if (!isLightOn)
	{
		// Update cache
		cachedLightState->brightness = LIFX_LIGHT_MAX_BRIGHTNESS;

		// Update light
		lifxClient->setColor(
				destinationMac,
				cachedLightState->hue,
				cachedLightState->saturation,
				cachedLightState->brightness,
				cachedLightState->kelvin,
				lightChangeDuration);
	}

	Serial.printf(
			"Light (%d%%) is currently %s, setting to %s...\n",
			LIFXClient::convertBrightnessLevelToPercentage(cachedLightState->brightness),
			isLightOn ? "on" : "off",
			!isLightOn ? "on" : "off");
	lifxClient->setPower(destinationMac, newLevel); // Update light
	cachedPowerState->level = newLevel;							// Cache new value
	lastLightToggle = millis();
}

void wifiSetUp()
{
	Serial.printf("Connecting to WiFi (SSID: %s)...\n", ssid);

	// Set WiFi to station mode and disconnect from an AP if it was previously connected
	WiFi.disconnect(true);
	delay(1000);
	WiFi.mode(WIFI_STA);

	// Try to connect
	WiFi.begin(ssid, pass);

	// Wait for connection or timeout
	unsigned int count = 0;
	while (WiFi.status() != WL_CONNECTED && count < 10)
	{
		Serial.print(".");
		count++;
		delay(1000);
	}

	if (WiFi.status() != WL_CONNECTED)
	{
		Serial.println("ERROR: WiFi connection failed!");
		delay(1000);
		wifiSetUp(); // Retry
		return;
	}

	delay(500);
	WiFi.setAutoReconnect(true);
	WiFi.persistent(true);
	Serial.printf("\nConnected, IP address: %s\n", WiFi.localIP().toString().c_str());
}