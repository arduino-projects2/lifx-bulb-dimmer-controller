#ifndef LIFXClient_h
#define LIFXClient_h

#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include "lifx-packets.h"
#include "lifx-payload-dtos.h"

class LIFXClient
{
private:
  uint32_t source;                           // Unqiue value per controller (user of this library)
  const int changeDuration = 750;            // Time taken for bulb colour/brightness/state transitions in ms
  const unsigned int timeoutInterval = 2000; // How to wait for messages from bulb
  const unsigned int localPort = 8888;       // Local port to listen for responses on
  void (*_deviceStatePowerHandler)(lifx_header, LIFXPowerState *);
  void (*_deviceStateLightHandler)(lifx_header, LIFXLightState *);
  void (*_deviceAcknowledgeHandler)(lifx_header);
  WiFiUDP wifiUdp;

  // Remote IP (In this case we broadcast to the entire subnet)
  const IPAddress broadcast_ip = IPAddress(255, 255, 255, 255);

  unsigned int currentSequence = 0; // Incrementing serial number of packet to LIFX bulb (0-255)

  lifx_header createHeader(uint8_t *destinationMac, uint16_t size, uint8_t res_required, uint8_t ack_required, uint16_t type);

public:
  static unsigned int convertBrightnessLevelToPercentage(uint16_t brightness);
  static uint16_t convertBooleanToPowerLevel(bool powerIsOn);
  static bool convertPowerLevelToBoolean(uint16_t level);
  LIFXClient(WiFiUDP wifiUdp, uint32_t source);

  void requestPower(uint8_t *destinationMac);
  void requestColor(uint8_t *destinationMac);

  void setPower(uint8_t *destinationMac, uint16_t level);
  void setColor(uint8_t *destinationMac, uint16_t hue, uint16_t saturation, uint16_t brightness, uint16_t kelvin, uint32_t duration);

  void processPackets();
  void setDeviceStatePowerHandler(void (*pointerToHandler)(lifx_header, LIFXPowerState *));
  void setDeviceStateLightHandler(void (*pointerToHandler)(lifx_header, LIFXLightState *));
  void setDeviceAcknowledgeHandler(void (*pointerToHandler)(lifx_header));
};

#endif