/*
  Sources:
  - https://community.lifx.com/t/sending-lan-packet-using-arduino/1460/4
  - https://lan.developer.lifx.com/docs/querying-the-device-for-data
  - https://lan.developer.lifx.com/docs/changing-a-device
*/


// The LIFX Header structure
#pragma pack(push, 1)
typedef struct
{
  /* frame */
  uint16_t size;
  uint16_t protocol : 12;
  uint8_t addressable : 1;
  uint8_t tagged : 1;
  uint8_t origin : 2;
  uint32_t source;
  /* frame address */
  uint8_t target[8];
  uint8_t reserved[6];
  uint8_t res_required : 1;
  uint8_t ack_required : 1;
  uint8_t : 6;
  uint8_t sequence;
  /* protocol header */
  uint64_t : 64;
  uint16_t type;
  uint16_t : 16;
  /* variable length payload follows */
} lifx_header;
#pragma pack(pop)


// Device::SetPower Payload
#pragma pack(push, 1)
typedef struct
{
  uint16_t level;
} lifx_payload_device_set_power;
#pragma pack(pop)

// Device::StatePower Payload
#pragma pack(push, 1)
typedef struct
{
  uint16_t level;
} lifx_payload_device_state_power;
#pragma pack(pop)


// Device::SetColor Payload
#pragma pack(push, 1)
typedef struct
{
  uint8_t reserved6;
  uint16_t hue;
  uint16_t saturation;
  uint16_t brightness;
  uint16_t kelvin;
  uint32_t duration;
} lifx_payload_device_set_color;
#pragma pack(pop)

// Device::StateColor Payload
#pragma pack(push, 1)
typedef struct
{
  uint16_t hue;
  uint16_t saturation;
  uint16_t brightness;
  uint16_t kelvin;
  uint16_t reserved6;
} lifx_payload_device_state_color;
#pragma pack(pop)


// Payload types
#define LIFX_DEVICE_GETPOWER 20
#define LIFX_DEVICE_SETPOWER 21
#define LIFX_DEVICE_STATEPOWER 22
#define LIFX_DEVICE_ACKNOWLEDGE 45
#define LIFX_DEVICE_GETCOLOR 101
#define LIFX_DEVICE_SETCOLOR 102
#define LIFX_DEVICE_STATELIGHT 107

// Packet buffer size
#define LIFX_INCOMING_PACKET_BUFFER_LEN 300

// Length in bytes of serial numbers
#define LIFX_SERIAL_LEN 6

#define LIFX_PORT 56700

#define LIFX_LIGHT_MIN_BRIGHTNESS 0
#define LIFX_LIGHT_MAX_BRIGHTNESS 65535
#define LIFX_POWER_LEVEL_OFF 0
#define LIFX_POWER_LEVEL_ON 65535